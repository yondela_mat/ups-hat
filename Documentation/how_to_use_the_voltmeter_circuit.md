## How to use the voltmeter circuit

1. Connect Vin to a 9V supply.
2. Connect 3.3V to the Op-Amp supply.
3. Then read off the voltage values sent to the Raspberry PI from the circuit.

With that the How-to guide has ended the Raspberry Pi should be reading off the
voltage that is being sent to it.
