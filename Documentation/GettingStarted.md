## Getting Started

1. Connect Vin to a 12V battery
2. Ensure that the PSU LED is on indicating that the battery is supplying properly
3. Ensure that the battery status LEDs correspond with what the raspiberry pi is indicating as the battery
level
4. Configure your raspberry pi to do a specific action if battery hits a certain level.
