## Here are the Manufacturing notes

1. Create the PCB using the following file https://gitlab.com/yondela_mat/ups-hat/-/blob/master/Schematics/UPS_Hat/UPS_Hat.kicad_pcb
2. Collect all the components (BOM) listed in this file https://gitlab.com/yondela_mat/ups-hat/-/blob/master/Schematics/UPS_Hat/UPS_Hat
3. Place all the components to their respective positions.
4. Solder every component to the board so that it is electrically connected.
