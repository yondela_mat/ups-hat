# UPS-HAT

## Project description

This project is on making a microHat that allows the Pi Zero to continue working when there is
a power outage and to gracefully shutdown running programs if there is a power outage.

# Sub-modules

## Voltmeter

This sub-module is focused on reading the voltage from the battery and being able to determine how
much life is left in the battery to continue running and if it is running low then we will need to ensure
to shutdown the Pi Zero.

## Power supply unit

## Status LEDs

# How to use UPS-HAT
