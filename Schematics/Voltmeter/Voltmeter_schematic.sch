EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Voltmeter circuit"
Date "2021-06-04"
Rev "0.2"
Comp "EEE3088F"
Comment1 "Author: Yondela"
Comment2 "This is the submodule that will be used in our UPS-HAT solution for the project."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:NCS325 U1
U 1 1 606BC6ED
P 4650 3150
F 0 "U1" H 4994 3196 50  0000 L CNN
F 1 "NCS325" H 4994 3105 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4650 3150 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NCS325-D.PDF" H 4800 3300 50  0001 C CNN
	1    4650 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2850 4550 2650
$Comp
L Device:R R1
U 1 1 606BE15E
P 3900 2750
F 0 "R1" H 3970 2796 50  0000 L CNN
F 1 "17.272k" H 3970 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 3830 2750 50  0001 C CNN
F 3 "~" H 3900 2750 50  0001 C CNN
	1    3900 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 606BEAAE
P 3900 3150
F 0 "R2" H 3970 3196 50  0000 L CNN
F 1 "10k" H 3970 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 3830 3150 50  0001 C CNN
F 3 "~" H 3900 3150 50  0001 C CNN
	1    3900 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2900 3900 2950
Wire Wire Line
	3900 2950 4200 2950
Wire Wire Line
	4200 2950 4200 3050
Wire Wire Line
	4200 3050 4350 3050
Connection ~ 3900 2950
Wire Wire Line
	3900 2950 3900 3000
Wire Wire Line
	4350 3250 4200 3250
Wire Wire Line
	4200 3850 4950 3850
Wire Wire Line
	4200 3250 4200 3850
Wire Wire Line
	4950 3850 4950 3150
$Comp
L Analog_ADC:MCP3004 U2
U 1 1 606C6692
P 6050 3250
F 0 "U2" H 6050 3831 50  0000 C CNN
F 1 "MCP3004" H 6050 3740 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_SMDSocket_SmallPads" H 6950 2950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21295C.pdf" H 6950 2950 50  0001 C CNN
	1    6050 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3300 3900 3550
Wire Wire Line
	6650 3150 6850 3150
Wire Wire Line
	6850 3250 6650 3250
Wire Wire Line
	6650 3350 6850 3350
Wire Wire Line
	6850 3450 6650 3450
Wire Wire Line
	6100 3900 6050 3900
Wire Wire Line
	6050 3900 6050 3750
Wire Wire Line
	5950 3750 5950 3900
Wire Wire Line
	5950 3900 6050 3900
Connection ~ 6050 3900
Wire Wire Line
	6100 2600 6050 2600
Wire Wire Line
	6050 2600 6050 2850
Wire Wire Line
	5950 2850 5950 2600
Wire Wire Line
	5950 2600 6050 2600
Connection ~ 6050 2600
Wire Wire Line
	4950 3150 5450 3150
Connection ~ 4950 3150
Wire Wire Line
	3900 2600 3900 2500
Text GLabel 6100 3900 2    50   Input ~ 0
GND
Text GLabel 4550 3600 3    50   Input ~ 0
GND
Text GLabel 3900 3550 3    50   Input ~ 0
GND
Text HLabel 3900 2500 1    50   Input ~ 0
9Vcc
Wire Wire Line
	4550 3600 4550 3450
Text HLabel 4550 2650 0    50   Input ~ 0
3.3Vcc
Text HLabel 6850 3150 2    50   Input ~ 0
GPIO_11_SCLK_Pin_23
Text HLabel 6850 3250 2    50   Input ~ 0
GPIO_09_MISO_Pin_21
Text HLabel 6850 3350 2    50   Input ~ 0
GPIO_10_MOSI_Pin_19
Text HLabel 6850 3450 2    50   Input ~ 0
GPIO_22_Pin_15
NoConn ~ 5450 3250
NoConn ~ 5450 3350
NoConn ~ 5450 3450
Text HLabel 6100 2600 2    50   Input ~ 0
3.3Vcc_raspberry
$EndSCHEMATC
